#include <moveit/move_group_interface/move_group_interface.h>
//#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

#include <trajectory_listener/PlannerDuration.h>

#include "rv7fr_planners_interface/pose_marker_hokudai_class.h"
int main(int argc, char** argv)
{
  ros::init(argc, argv, "simple_move_group");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();
  
  ROS_INFO("COOL HERE");

  static const std::string PLANNING_GROUP = "arm";
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
//  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  const moveit::core::JointModelGroup* joint_model_group = 
    move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);


  //Visualization
  namespace rvt = rviz_visual_tools;
  moveit_visual_tools::MoveItVisualTools visual_tools("table");
  visual_tools.deleteAllMarkers();

  std::copy(move_group.getJointModelGroupNames().begin(), move_group.getJointModelGroupNames().end(),
            std::ostream_iterator<std::string>(std::cout, ", "));

  //visual_tools.loadRemoteControl();

  
  Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
  text_pose.translation().z() = 1.75;
  
  PoseMarker markers("rv7");

  geometry_msgs::Pose start_pose;
  start_pose.orientation.x = 0.707083360325;
  start_pose.orientation.y = 5.17992987498e-05;
  start_pose.orientation.z = 0.707130199162;
  start_pose.orientation.w = 1.73810759282e-05;
  start_pose.position.x = 0.429439;
  start_pose.position.y = 0.246871;
  start_pose.position.z = 0.4162;

  geometry_msgs::Pose pose1;
  pose1.orientation.x = 0.707214994375;
  pose1.orientation.y = -4.57488265851e-5;
  pose1.orientation.z = 0.706998495513;
  pose1.orientation.w = 0.000277452732221;
  pose1.position.x = 0.456462;
  pose1.position.y = -0.289627;
  pose1.position.z = 0.427991;

  geometry_msgs::Pose pose2;
  pose2.orientation.x = 0.707290371072;
  pose2.orientation.y = -8.81092724885e-5;
  pose2.orientation.z = 0.706923134122;
  pose2.orientation.w = 7.52963067658e-5;
  pose2.position.x = 0.591001115432;
  pose2.position.y = -0.31478017123;
  pose2.position.z = 1.00483301414;
/*
  moveit_msgs::OrientationConstraint ocm;
  ocm.link_name = "link_effector";
  ocm.header.frame_id = "world";
  ocm.orientation.x = -0.707214994375;
  ocm.orientation.y = -4.57488265851e-5;
  ocm.orientation.z = -0.706998495513;
  ocm.orientation.w = 0.000277452732221;
  ocm.absolute_x_axis_tolerance = 0.005;
  ocm.absolute_y_axis_tolerance = 0.005;
  ocm.absolute_z_axis_tolerance = 0.005;
  ocm.weight = 1.0;

  // Now, set it as the path constraint for the group.
  moveit_msgs::Constraints test_constraints;
  test_constraints.orientation_constraints.push_back(ocm);
  move_group.setPathConstraints(test_constraints);

*/
  int iteration=0;
  
  bool success;
  moveit::planning_interface::MoveGroupInterface::Plan plan1;
  const std::string& plannerID(move_group.getPlannerId());
  move_group.setPlannerId("RRT");

//time  uint32_t planner_process_duration;
//time  uint32_t pre_planning_time;
//time  uint32_t post_planning_time;

  ros::Duration planner_process_duration;
  ros::Time pre_planning_time;
  ros::Time post_planning_time;
  ros::Publisher duration_pub = node_handle.advertise<trajectory_listener::PlannerDuration>("duration_publisher", 1000); 
  trajectory_listener::PlannerDuration duration_msg;

  while(iteration<10)
  {
    //Start plan and move to initial pose
    ROS_INFO("Current Planner ID:%s", plannerID.c_str());
    move_group.setPoseTarget(markers.getStart());
    
    //Start timing
    pre_planning_time = ros::Time::now();
    success = (move_group.plan(plan1) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    post_planning_time = ros::Time::now();
    planner_process_duration = post_planning_time - pre_planning_time;
    
    ROS_INFO_STREAM("Planner process time:" << planner_process_duration.toSec() << " s"); 
    ROS_INFO_STREAM("Planner process time:" << planner_process_duration.toNSec() << " ns"); 
    planner_process_duration = post_planning_time-pre_planning_time;
    
    duration_msg.header.stamp = ros::Time::now();
    duration_msg.nsecs = planner_process_duration.toNSec();
    duration_msg.secs =  planner_process_duration.toSec();
    duration_pub.publish(duration_msg);

    visual_tools.publishTrajectoryLine(plan1.trajectory_, joint_model_group); //[IMAN] [KIV] disable?
    //visual_tools.trigger();
    ROS_INFO_NAMED("RV7FR", "Pose Goal Plan 1 %s", success ? "" : "FAILED");
    if(success) move_group.execute(plan1.trajectory_);
 
   //Start plan and move to goal pose 
    move_group.setPoseTarget(markers.getEnd());

    pre_planning_time = ros::Time::now();
    success = (move_group.plan(plan1) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    post_planning_time = ros::Time::now();
    planner_process_duration = post_planning_time - pre_planning_time;

    duration_msg.header.stamp = ros::Time::now();
    duration_msg.nsecs = planner_process_duration.toNSec();
    duration_msg.secs =  planner_process_duration.toSec();
    duration_pub.publish(duration_msg);

    ROS_INFO_NAMED("RV7FR", "Pose Goal Plan 2 %s", success ? "" : "FAILED");
    if(success) move_group.execute(plan1.trajectory_);
    
    ++iteration;
    ROS_INFO_STREAM("End Cycle: "<< iteration);
  }

  ros::shutdown();
  return 0;
}
